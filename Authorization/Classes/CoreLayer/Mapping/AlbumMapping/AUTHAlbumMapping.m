//
//  AUTHAlbumMapping.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAlbumMapping.h"
#import "AUTHObjectManager.h"
#import "Album+CoreDataClass.h"
#import "AUTHMappingDictionaries.h"

@implementation AUTHAlbumMapping

+ (void)setupMapping {
    RKEntityMapping *albumMapping = [self albumMapping];
    [[AUTHObjectManager sharedManager] addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:albumMapping
                                                                                                          method:RKRequestMethodGET pathPattern:[User currentUser].albumListAPIEndpoint
                                                                                                         keyPath:nil
                                                                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
}

+ (RKEntityMapping *)albumMapping {
    static RKEntityMapping *albumMapping;
    if (!albumMapping) {
        albumMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Album class])
                                           inManagedObjectStore:[AUTHObjectManager sharedManager].managedObjectStore];
        [albumMapping addAttributeMappingsFromDictionary:[AUTHMappingDictionaries albumMappingDictionary]];
        albumMapping.identificationAttributes = @[@"albumId"];
    }
    return albumMapping;
}

@end
