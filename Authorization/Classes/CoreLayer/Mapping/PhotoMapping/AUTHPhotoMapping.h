//
//  AUTHPhotoMapping.h
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AUTHPhotoMapping : NSObject

+ (void)setupMapping;
+ (RKEntityMapping *)photoMapping;

@end
