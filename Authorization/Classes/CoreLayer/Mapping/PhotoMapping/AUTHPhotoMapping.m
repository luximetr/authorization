//
//  AUTHPhotoMapping.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHPhotoMapping.h"
#import "AUTHObjectManager.h"
#import "Photo+CoreDataClass.h"
#import "AUTHMappingDictionaries.h"

@implementation AUTHPhotoMapping

+ (void)setupMapping {
    RKEntityMapping *photoMapping = [self photoMapping];
    [[AUTHObjectManager sharedManager] addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:photoMapping
                                                                                                          method:RKRequestMethodGET pathPattern:[User currentUser].photoListAPIEndpoint
                                                                                                         keyPath:nil
                                                                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
}

+ (RKEntityMapping *)photoMapping {
    static RKEntityMapping *photoMapping;
    if (!photoMapping) {
        photoMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Photo class])
                                           inManagedObjectStore:[AUTHObjectManager sharedManager].managedObjectStore];
        [photoMapping addAttributeMappingsFromDictionary:[AUTHMappingDictionaries photoMappingDictionary]];
        photoMapping.identificationAttributes = @[@"photoId"];
    }
    return photoMapping;
}

@end
