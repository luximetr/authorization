//
//  AUTHMappingDictionaries.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHMappingDictionaries.h"
#import "Album+CoreDataClass.h"
#import "Photo+CoreDataClass.h"

@implementation AUTHMappingDictionaries

+ (NSDictionary *)albumMappingDictionary {
    return @{
             @"edited"      : NSStringFromSelector(@selector(edited)),
             @"updated"     : NSStringFromSelector(@selector(updated)),
             @"id"          : NSStringFromSelector(@selector(albumId)),
             @"title"       : NSStringFromSelector(@selector(title)),
             @"published"   : NSStringFromSelector(@selector(published)),
             @"imageCount"  : NSStringFromSelector(@selector(imageCount))
             };
}

+ (NSDictionary *)photoMappingDictionary {
    return @{
             @"id"          : NSStringFromSelector(@selector(photoId)),
             @"edited"      : NSStringFromSelector(@selector(edited)),
             @"updated"     : NSStringFromSelector(@selector(updated)),
             @"img"         : NSStringFromSelector(@selector(image)),
             @"published"   : NSStringFromSelector(@selector(published))
             };
}

@end
