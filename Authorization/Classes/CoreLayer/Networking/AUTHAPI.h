//
//  AUTHAPI.h
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AUTHAPISuccessErrorCompletion)(NSError *error);
typedef void (^AUTHAPISuccessErrorCompletionBlockWithToken)(NSError *error, id JSON);

@interface AUTHAPI : NSObject

#pragma mark - Authorization

+ (void)receiveApplicationTokenWithCode:(NSNumber *)code completion:(AUTHAPISuccessErrorCompletion)completion;
+ (void)getServiceDocumentOnComplete:(AUTHAPISuccessErrorCompletion)completion;

#pragma mark - Albums

+ (void)getAlbumsWithNumberOnCompletion:(AUTHAPISuccessErrorCompletion)completion;

#pragma mark - Photos

+ (void)getPhotosOnComplete:(AUTHAPISuccessErrorCompletion)completion;

@end
