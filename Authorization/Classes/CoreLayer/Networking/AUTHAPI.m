//
//  AUTHAPI.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAPI.h"
#import "AUTHObjectManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "User+CoreDataClass.h"
#import "AFNetworking.h"

@implementation AUTHAPI

#pragma mark - Authorization

+ (void)receiveApplicationTokenWithCode:(NSNumber *)code completion:(AUTHAPISuccessErrorCompletion)completion {
    AUTHAPISuccessErrorCompletion completionCopy = [completion copy];
    
    NSDictionary *body = @{@"grant_type"     : @"authorization_code",
                           @"code"           : code,
                           @"client_id"      : AUTHApplicationId,
                           @"client_secret"  : AUTHApplicationPassword
                                 };
    NSMutableString *stringBody = [[NSMutableString alloc] init];
    for (NSString *key in body.allKeys) {
        [stringBody appendFormat:@"%@=%@", key, body[key]];
        if (![key isEqualToString:body.allKeys.lastObject]) {
            [stringBody appendFormat:@"&"];
        }
    }
    
    NSData *dataBody = [stringBody dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:AUTHTokenReceiveEndpoint relativeToURL:[NSURL URLWithString:AUTHAuthorizationHostname]]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:dataBody];
    
    AFRKJSONRequestOperation *operation = [AFRKJSONRequestOperation
                                           JSONRequestOperationWithRequest:request
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                               [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                                   [User MR_truncateAllInContext:localContext];
                                                   User *newUser = [User MR_createEntityInContext:localContext];
                                                   newUser.isCurrentUser = @(YES);
                                                   newUser.accessToken   = JSON[@"access_token"];
                                               } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                                   NSLog(@"users number: %lu", [User MR_numberOfEntities].integerValue);
                                                   if (completionCopy) {
                                                       completionCopy(nil);
                                                   }
                                               }];
                                           }
                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                               if (completionCopy) {
                                                   completionCopy(error);
                                               }
                                           }];
    operation.allowsInvalidSSLCertificate = YES;
    
    [operation start];
}

+ (void)getServiceDocumentOnComplete:(AUTHAPISuccessErrorCompletion)completion {
    AUTHAPISuccessErrorCompletion completionCopy = [completion copy];
    
    NSURLRequest *request = [[AUTHObjectManager sharedManager] requestWithObject:nil
                                                                          method:RKRequestMethodGET
                                                                            path:AUTHServiceDocumentReceiveEndpoint
                                                                      parameters:nil];
    
    NSString *certPath = [[NSBundle mainBundle] pathForResource:@"ycasha2" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:certPath];
    NSURLSessionConfiguration *configurator = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *client = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configurator];
    client.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    [client.securityPolicy setPinnedCertificates:[NSSet setWithObject:certData]];
    client.securityPolicy.allowInvalidCertificates = YES;
    
    [client setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[client dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                User *localUser = [User currentUserInContext:localContext];
                localUser.photoListAPIEndpoint = [responseObject[@"collections"][@"photo-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
                localUser.albumListAPIEndpoint = [responseObject[@"collections"][@"album-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
                localUser.tagListAPIEndpoint   = [responseObject[@"collections"][@"tags-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                if (completionCopy) {
                    completionCopy(error);
                }
            }];
        }
        else {
            if (completionCopy) {
                completionCopy(error);
            }
        }
    }] resume];
    
    /*AFRKJSONRequestOperation *operation = [AFRKJSONRequestOperation
                                           JSONRequestOperationWithRequest:request
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                               [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                                   User *localUser = [User currentUserInContext:localContext];
                                                   localUser.photoListAPIEndpoint = [JSON[@"collections"][@"photo-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
                                                   localUser.albumListAPIEndpoint = [JSON[@"collections"][@"album-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
                                                   localUser.tagListAPIEndpoint   = [JSON[@"collections"][@"tags-list"][@"href"] stringByReplacingOccurrencesOfString:AUTHServerHostname withString:@""];
                                               } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                                   if (completionCopy) {
                                                       completionCopy(error);
                                                   }
                                               }];
                                               
                                           }
                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                               if (completionCopy) {
                                                   completionCopy(error);
                                               }
                                           }];
    
    [operation start];*/
}

#pragma mark - Albums

+ (void)getAlbumsWithNumberOnCompletion:(AUTHAPISuccessErrorCompletion)completion {
    AUTHAPISuccessErrorCompletion completionCopy = [completion copy];
    
    NSURLRequest *request = [[AUTHObjectManager sharedManager] requestWithObject:nil method:RKRequestMethodGET path:[User currentUser].albumListAPIEndpoint parameters:nil];
    RKManagedObjectRequestOperation *operation =
    [[AUTHObjectManager sharedManager] managedObjectRequestOperationWithRequest:request
                                                           managedObjectContext:[[[AUTHObjectManager sharedManager] managedObjectStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO]
                                                                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                            if (completionCopy) {
                                                                                completionCopy(nil);
                                                                            }
                                                                        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                            if (completionCopy) {
                                                                                completionCopy(error);
                                                                            }
                                                                        }];
    [operation setWillMapDeserializedResponseBlock:^id(id deserializedResponseBody) {
        return deserializedResponseBody[@"entries"];
    }];
    
    [[AUTHObjectManager sharedManager] enqueueObjectRequestOperation:operation];
}

#pragma mark - Photos

+ (void)getPhotosOnComplete:(AUTHAPISuccessErrorCompletion)completion {
    AUTHAPISuccessErrorCompletion completionCopy = [completion copy];
    
    NSURLRequest *request = [[AUTHObjectManager sharedManager] requestWithObject:nil method:RKRequestMethodGET path:[User currentUser].photoListAPIEndpoint parameters:nil];
    
    NSString *certPath = [[NSBundle mainBundle] pathForResource:@"ycasha2" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:certPath];
    /*NSURLSessionConfiguration *configurator = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *client = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configurator];
    client.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    [client.securityPolicy setPinnedCertificates:[NSSet setWithObject:certData]];
    client.securityPolicy.allowInvalidCertificates = NO;
    
    [client setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[client dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSLog(@"%@", responseObject);
    }] resume];*/
    
    RKManagedObjectRequestOperation *operation =
    [[AUTHObjectManager sharedManager] managedObjectRequestOperationWithRequest:request
                                                           managedObjectContext:[[[AUTHObjectManager sharedManager] managedObjectStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO]
                                                                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                            if (completionCopy) {
                                                                                completionCopy(nil);
                                                                            }
                                                                        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                            if (completionCopy) {
                                                                                completionCopy(error);
                                                                            }
                                                                        }];
    
    [operation setWillMapDeserializedResponseBlock:^id(id deserializedResponseBody) {
        return deserializedResponseBody[@"entries"];
    }];
    
    [[AUTHObjectManager sharedManager] enqueueObjectRequestOperation:operation];
}

@end
