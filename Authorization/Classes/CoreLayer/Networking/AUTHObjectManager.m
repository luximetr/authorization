//
//  AUTHObjectManager.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHObjectManager.h"
#import "User+CoreDataClass.h"

@implementation AUTHObjectManager

#pragma mark - Overrided

- (NSMutableURLRequest *)requestWithObject:(id)object
                                    method:(RKRequestMethod)method
                                      path:(NSString *)path
                                parameters:(NSDictionary *)parameters {
    NSMutableURLRequest *request = [super requestWithObject:object method:method path:path parameters:parameters];
    [self addCustomHeadersToRequest:request];
    request.timeoutInterval = 60;
    return request;
}

- (NSMutableURLRequest *)requestWithPathForRouteNamed:(NSString *)routeName
                                               object:(id)object
                                           parameters:(NSDictionary *)parameters {
    NSMutableURLRequest *request = [super requestWithPathForRouteNamed:routeName object:object parameters:parameters];
    [self addCustomHeadersToRequest:request];
    request.timeoutInterval = 60;
    return request;
}

- (NSMutableURLRequest *)multipartFormRequestWithObject:(id)object
                                                 method:(RKRequestMethod)method
                                                   path:(NSString *)path
                                             parameters:(NSDictionary *)parameters
                              constructingBodyWithBlock:(void (^)(id<AFRKMultipartFormData>))block {
    
    NSMutableURLRequest *request = [super multipartFormRequestWithObject:object method:method path:path parameters:parameters constructingBodyWithBlock:block];
    if([User currentUser].accessToken) {
        [request setValue:[NSString stringWithFormat:@"OAuth %@", [User currentUser].accessToken] forHTTPHeaderField:@"Authorization"];
    }
    request.timeoutInterval = 60;
    return request;
}

- (void)addCustomHeadersToRequest:(NSMutableURLRequest*)request {
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if([User currentUser].accessToken) {
        [request setValue:[NSString stringWithFormat:@"OAuth %@", [User currentUser].accessToken] forHTTPHeaderField:@"Authorization"];
        NSLog(@"%@", [NSString stringWithFormat:@"OAuth %@", [User currentUser].accessToken]);
    }
    
}
@end
