//
//  AUTHPathBuilder.m
//  Authorization
//
//  Created by Alexandr Orlov on 20.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHPathBuilder.h"

static const int kKeyIndex   = 0;
static const int kValueIndex = 1;

@implementation AUTHPathBuilder

+ (instancetype)sharedBuilder {
    static AUTHPathBuilder *builder = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        builder = [[AUTHPathBuilder alloc] init];
    });
    
    return builder;
}

- (NSURL *)constructURL:(NSURL *)url withParameters:(NSDictionary *)parameters {
    return [NSURL URLWithString:[[url absoluteString] stringByAppendingFormat:@"?%@", [self stringWithParameters:parameters]]];
}

- (NSString *)stringWithParameters:(NSDictionary *)parameters {
    NSMutableString *resultStringWithParameters = [[NSMutableString alloc] init];
    for(NSString *key in parameters.allKeys) {
        [resultStringWithParameters appendFormat:@"%@=%@", key, parameters[key]];
        if (![key isEqualToString:parameters.allKeys.lastObject]) {
            [resultStringWithParameters appendString:@"&"];
        }
    }
    return resultStringWithParameters;
}

- (NSDictionary *)parametersFromURL:(NSURL *)url {
    NSMutableDictionary *parametersResult = [[NSMutableDictionary alloc] init];
    NSString *absoluteString = url.absoluteString;
    if (![absoluteString containsString:@"?"]) {
        return nil;
    }
    
    NSString *parametersString = [absoluteString componentsSeparatedByString:@"?"][1];
    if ([parametersString containsString:@"&"]) {
        NSArray *parametersPairs = [parametersString componentsSeparatedByString:@"&"];
        for(NSString *pair in parametersPairs) {
            NSString *key   = [pair componentsSeparatedByString:@"="][kKeyIndex];
            NSString *value = [pair componentsSeparatedByString:@"="][kValueIndex];
            [parametersResult setValue:value forKey:key];
        }
    }
    else {
        NSString *key = [parametersString componentsSeparatedByString:@"="][kKeyIndex];
        NSString *value = [parametersString componentsSeparatedByString:@"="][kValueIndex];
        [parametersResult setValue:value forKey:key];
    }
    return parametersResult;
}

- (NSNumber *)verificationCodeFromURL:(NSURL *)url {
    NSDictionary *parameters = [self parametersFromURL:url];
    if (parameters[@"error"]) {
        NSLog(@"%@", parameters[@"error"]);
        NSLog(@"%@", parameters[@"error_description"]);
        return nil;
    }
    else {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        return [formatter numberFromString:parameters[@"code"]];
    }
}

@end
