//
//  AUTHPathBuilder.h
//  Authorization
//
//  Created by Alexandr Orlov on 20.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AUTHPathBuilder : NSObject

+ (instancetype)sharedBuilder;
- (NSURL *)constructURL:(NSURL *)url withParameters:(NSDictionary *)parameters;
- (NSString *)stringWithParameters:(NSDictionary *)parameters;
- (NSDictionary *)parametersFromURL:(NSURL *)url;
- (NSNumber *)verificationCodeFromURL:(NSURL *)url;

@end
