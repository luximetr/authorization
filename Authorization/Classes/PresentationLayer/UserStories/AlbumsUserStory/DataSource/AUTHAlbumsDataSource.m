//
//  AUTHAlbumsDataSource.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAlbumsDataSource.h"
#import "AUTHAlbumCollectionViewCell.h"
#import "AUTHPhotoCollectionViewCell.h"
#import "Album+CoreDataClass.h"
#import "Photo+CoreDataClass.h"

@interface AUTHAlbumsDataSource () <UICollectionViewDataSource>

@end

@implementation AUTHAlbumsDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albumArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id item = self.albumArray[indexPath.item];
    UICollectionViewCell * cell = nil;
    if ([item isKindOfClass:[Album class]]) {
        cell = (AUTHAlbumCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:AUTHAlbumCollectionViewCellIdentifier forIndexPath:indexPath];
        [(AUTHAlbumCollectionViewCell *)cell configureWithImageString:@"http://www.freeiconspng.com/uploads/photo-album-icon-png-14.png" title:[self.albumArray[indexPath.item] valueForKey:@"title"]];
    }
    else if ([item isKindOfClass:[Photo class]]) {
        cell = (AUTHPhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:AUTHPhotoCollectionViewCellIdentifier forIndexPath:indexPath];
        NSString *originalPhotoString = [NSKeyedUnarchiver unarchiveObjectWithData:((Photo *)item).image][@"orig"][@"href"];
        [(AUTHPhotoCollectionViewCell *)cell configureWithImageString:originalPhotoString];
    }
    
    return cell;
}



@end
