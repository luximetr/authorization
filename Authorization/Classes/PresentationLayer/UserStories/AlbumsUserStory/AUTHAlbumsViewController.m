//
//  AUTHAlbumsViewController.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAlbumsViewController.h"
#import "AUTHAlbumCollectionViewCell.h"
#import "AUTHPhotoCollectionViewCell.h"
#import "AUTHAlbumsDataSource.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Album+CoreDataClass.h"
#import "Photo+CoreDataClass.h"

@interface AUTHAlbumsViewController () <UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *albumsCollectionView;

@end

@implementation AUTHAlbumsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBarUI];
    
    [self.albumsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([AUTHAlbumCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:AUTHAlbumCollectionViewCellIdentifier];
    [self.albumsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([AUTHPhotoCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:AUTHPhotoCollectionViewCellIdentifier];
    
    NSLog(@"%@ %@", [User currentUser].albumListAPIEndpoint, [User currentUser].photoListAPIEndpoint);
    
    [AUTHAPI getPhotosOnComplete:^(NSError *error) {
        ((AUTHAlbumsDataSource *)self.albumsCollectionView.dataSource).albumArray = [Photo MR_findAll];
        [self.albumsCollectionView reloadData];
    }];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([((AUTHAlbumsDataSource *)self.albumsCollectionView.dataSource).albumArray[indexPath.item] isKindOfClass:[Album class]]) {
        // open album
        // [self.albumsCollectionView reloadData];
    }
}

#pragma mark - IBActions

- (IBAction)logoutButtoPressed:(id)sender {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        [User MR_truncateAllInContext:localContext];
        [Album MR_truncateAllInContext:localContext];
        [Photo MR_truncateAllInContext:localContext];
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        [self performSegueWithIdentifier:AUTHUnwindToAuthorizationViewControllerIdentifier sender:self];
    }];
}

#pragma mark - Private

- (void)setupNavigationBarUI {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationItem.leftBarButtonItem = nil;
}


@end
