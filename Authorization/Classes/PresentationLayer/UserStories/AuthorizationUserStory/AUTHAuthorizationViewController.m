//
//  AUTHAuthorizationViewController.m
//  Authorization
//
//  Created by Alexandr Orlov on 19.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAuthorizationViewController.h"
#import "AUTHPathBuilder.h"
#import "AUTHAuthorization.h"
#import <MagicalRecord/MagicalRecord.h>
#import "User+CoreDataClass.h"

@interface AUTHAuthorizationViewController ()

@end

@implementation AUTHAuthorizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBarUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBackFromAuthorizationNotificaion:) name:AUTHDidBackFromAuthorizationNotificationName object:nil];
}

- (void)dealloc {
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AUTHDidBackFromAuthorizationNotificationName object:nil];
    } @catch (NSException *exception) {
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    [AUTHAuthorization authorizeInBrowser];
}

- (void)didBackFromAuthorizationNotificaion:(NSNotification *)notificaiton {
    if ([notificaiton.object isKindOfClass:[NSURL class]]) {
        NSNumber *code = [[AUTHPathBuilder sharedBuilder] verificationCodeFromURL:notificaiton.object];
        NSLog(@"CODE: %@ (reduce in 10 minutes)", code);
        
        [AUTHAPI receiveApplicationTokenWithCode:code completion:^(NSError *error) {
            if (!error) {
                [AUTHAPI getServiceDocumentOnComplete:^(NSError *error) {
                    if (!error) {
                        NSLog(@"Current user state: %@ %@ %@", [User currentUser].albumListAPIEndpoint, [User currentUser].photoListAPIEndpoint, [User currentUser].tagListAPIEndpoint);
                       
                        [self performSegueWithIdentifier:AUTHAlbumsViewControllerShowSegueIdentifier sender:self];
                    }
                }];
            }
        }];
    }
}

- (IBAction)unwindFromAlbums:(UIStoryboardSegue *)segue {
}


#pragma mark - Private

- (void)setupNavigationBarUI {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationItem.leftBarButtonItem = nil;
}

@end
