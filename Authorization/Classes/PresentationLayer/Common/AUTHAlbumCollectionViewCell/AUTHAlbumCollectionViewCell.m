//
//  AUTHAlbumCollectionViewCell.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAlbumCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AUTHAlbumCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *albumTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation AUTHAlbumCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.albumTitleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)configureWithImageString:(NSString *)imageString title:(NSString *)title {
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://www.freeiconspng.com/uploads/photo-album-icon-png-14.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSLog(@"%@", error);
    }];
    self.albumTitleLabel.text = title;
}
@end
