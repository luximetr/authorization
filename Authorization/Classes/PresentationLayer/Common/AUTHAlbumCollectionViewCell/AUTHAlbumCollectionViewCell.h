//
//  AUTHAlbumCollectionViewCell.h
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AUTHAlbumCollectionViewCell : UICollectionViewCell

- (void)configureWithImageString:(NSString *)imageString title:(NSString *)title;

@end
