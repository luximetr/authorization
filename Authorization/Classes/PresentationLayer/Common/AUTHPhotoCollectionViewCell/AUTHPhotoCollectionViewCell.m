//
//  AUTHPhotoCollectionViewCell.m
//  Authorization
//
//  Created by Alexandr Orlov on 26.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHPhotoCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AUTHPhotoCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end

@implementation AUTHPhotoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configureWithImageString:(NSString *)imageString {
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:imageString]];
}

@end
