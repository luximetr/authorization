//
//  User+CoreDataProperties.m
//  
//
//  Created by Alexandr Orlov on 26.10.16.
//
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"User"];
}

@dynamic accessToken;
@dynamic albumListAPIEndpoint;
@dynamic isCurrentUser;
@dynamic photoListAPIEndpoint;
@dynamic tagListAPIEndpoint;

@end
