//
//  Photo+CoreDataProperties.h
//  
//
//  Created by Alexandr Orlov on 26.10.16.
//
//  This file was automatically generated and should not be edited.
//

#import "Photo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Photo (CoreDataProperties)

+ (NSFetchRequest<Photo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *photoId;
@property (nullable, nonatomic, copy) NSDate *edited;
@property (nullable, nonatomic, copy) NSDate *updated;
@property (nullable, nonatomic, copy) NSDate *published;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
