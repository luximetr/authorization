//
//  User+CoreDataProperties.h
//  
//
//  Created by Alexandr Orlov on 26.10.16.
//
//

#import "User+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *accessToken;
@property (nullable, nonatomic, copy) NSString *albumListAPIEndpoint;
@property (nonatomic) BOOL isCurrentUser;
@property (nullable, nonatomic, copy) NSString *photoListAPIEndpoint;
@property (nullable, nonatomic, copy) NSString *tagListAPIEndpoint;

@end

NS_ASSUME_NONNULL_END
