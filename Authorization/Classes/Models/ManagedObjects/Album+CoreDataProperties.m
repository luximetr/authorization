//
//  Album+CoreDataProperties.m
//  
//
//  Created by Alexandr Orlov on 11.11.16.
//
//  This file was automatically generated and should not be edited.
//

#import "Album+CoreDataProperties.h"

@implementation Album (CoreDataProperties)

+ (NSFetchRequest<Album *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Album"];
}

@dynamic albumId;
@dynamic edited;
@dynamic imageCount;
@dynamic published;
@dynamic title;
@dynamic updated;

@end
