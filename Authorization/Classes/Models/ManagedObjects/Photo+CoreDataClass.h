//
//  Photo+CoreDataClass.h
//  
//
//  Created by Alexandr Orlov on 26.10.16.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Photo : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Photo+CoreDataProperties.h"
