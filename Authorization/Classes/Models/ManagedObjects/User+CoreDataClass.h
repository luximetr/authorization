//
//  User+CoreDataClass.h
//  
//
//  Created by Alexandr Orlov on 25.10.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

+ (User *)currentUser;
+ (User *)currentUserInContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
