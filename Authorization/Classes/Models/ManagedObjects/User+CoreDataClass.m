//
//  User+CoreDataClass.m
//  
//
//  Created by Alexandr Orlov on 25.10.16.
//
//

#import "User+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>


@implementation User

+ (User *)currentUser {
    return [User MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"isCurrentUser == YES"]];
}

+ (User *)currentUserInContext:(NSManagedObjectContext *)context {
    return [User MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"isCurrentUser == YES"] inContext:context];
}

@end
