//
//  Album+CoreDataProperties.h
//  
//
//  Created by Alexandr Orlov on 11.11.16.
//
//  This file was automatically generated and should not be edited.
//

#import "Album+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Album (CoreDataProperties)

+ (NSFetchRequest<Album *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *albumId;
@property (nullable, nonatomic, copy) NSDate *edited;
@property (nonatomic) int32_t imageCount;
@property (nullable, nonatomic, copy) NSDate *published;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSDate *updated;

@end

NS_ASSUME_NONNULL_END
