//
//  Photo+CoreDataProperties.m
//  
//
//  Created by Alexandr Orlov on 26.10.16.
//
//  This file was automatically generated and should not be edited.
//

#import "Photo+CoreDataProperties.h"

@implementation Photo (CoreDataProperties)

+ (NSFetchRequest<Photo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
}

@dynamic photoId;
@dynamic edited;
@dynamic updated;
@dynamic published;
@dynamic image;
@dynamic title;

@end
