//
//  AUTHApplicationConfigurator.h
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AUTHApplicationConfigurator : NSObject

+ (void)setupCoreDataStack;

@end
