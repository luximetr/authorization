//
//  AUTHApplicationConfigurator.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHApplicationConfigurator.h"
#import <CoreData/CoreData.h>
#import <MagicalRecord/MagicalRecord.h>
#import "NSManagedObjectContext+MagicalRecordRestKit.h"
#import <RestKit/CoreData.h>
#import <RestKit/RestKit.h>
#import "AUTHObjectManager.h"

#import "AUTHAlbumMapping.h"
#import "AUTHPhotoMapping.h"

@implementation AUTHApplicationConfigurator

+ (void)setupCoreDataStack {
    NSURL *baseURL = [NSURL URLWithString:AUTHServerHostname];
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    if (!managedObjectModel) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Auth" withExtension:@"momd"];
        managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    AUTHObjectManager *objectManager = [AUTHObjectManager managerWithBaseURL:baseURL];
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:RKMIMETypeJSON];
    
    //remove current settings
    [objectManager.responseDescriptors enumerateObjectsUsingBlock:^(RKResponseDescriptor *responseDescriptor, NSUInteger idx, BOOL *stop) {
        [objectManager removeResponseDescriptor:responseDescriptor];
    }];
    
    objectManager.managedObjectStore = managedObjectStore;
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @YES,
                               NSInferMappingModelAutomaticallyOption : @YES
                               };
    NSString *persistentStoreName = @"AUTHBase";
    NSString *storePath = [[AUTHApplicationConfigurator applicationSupportDirectory] stringByAppendingPathComponent:[persistentStoreName stringByAppendingPathExtension:@"sqlite"]];
    NSError *error;
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:options error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    // Setup Magical Record with managed object model
    [NSManagedObjectModel MR_setDefaultManagedObjectModel:managedObjectStore.managedObjectModel];
    
    // Setup Magical Record with persistent store coordinator
    [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:managedObjectStore.persistentStoreCoordinator];
    
    // Setup Magical Record with managed object contexts
    [NSManagedObjectContext MR_setDefaultContext:managedObjectStore.mainQueueManagedObjectContext];
    [NSManagedObjectContext MR_setRootSavingContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [RKFetchRequestManagedObjectCache new];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^() {
        //[AUTHObjectManager setupRoutes];
        [AUTHAlbumMapping setupMapping];
        [AUTHPhotoMapping setupMapping];
    });
}

+ (NSString *)applicationSupportDirectory {
    NSFileManager *sharedFM = [NSFileManager defaultManager];
    
    NSArray *possibleURLs = [sharedFM URLsForDirectory:NSApplicationSupportDirectory
                                             inDomains:NSUserDomainMask];
    NSURL *appSupportDir = nil;
    NSURL *appDirectory = nil;
    
    if ([possibleURLs count] >= 1) {
        appSupportDir = [possibleURLs objectAtIndex:0];
    }
    
    if (appSupportDir) {
        appDirectory = [appSupportDir URLByAppendingPathComponent:[[[NSBundle mainBundle] executablePath] lastPathComponent]];
        
        BOOL isDirectory;
        if (![sharedFM fileExistsAtPath:appDirectory.path isDirectory:&isDirectory]) {
            NSError *error;
            [sharedFM createDirectoryAtPath:appDirectory.path withIntermediateDirectories:YES attributes:nil error:&error];
            
            if (error) {
                return nil;
            }
        }
        
        return [appDirectory path];
    }
    return nil;
}

@end
