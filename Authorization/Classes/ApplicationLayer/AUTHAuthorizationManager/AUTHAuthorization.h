//
//  AUTHAuthorization.h
//  Authorization
//
//  Created by Alexandr Orlov on 20.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString * const AUTHCallbackURLString;

@interface AUTHAuthorization : NSObject

+ (void)authorizeInWebView:(UIWebView *)webView;
+ (void)authorizeInBrowser;

@end

