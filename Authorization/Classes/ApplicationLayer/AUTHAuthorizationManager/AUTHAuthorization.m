//
//  AUTHAuthorization.m
//  Authorization
//
//  Created by Alexandr Orlov on 20.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAuthorization.h"
#import "AUTHPathBuilder.h"
#import "AUTHObjectManager.h"

NSString * const AUTHResponseType          = @"code";

@implementation AUTHAuthorization

+ (void)authorizeInWebView:(UIWebView *)webView {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[self authorizeURL]];
    [request setHTTPMethod:@"POST"];
    [webView loadRequest:request];
}

+ (void)authorizeInBrowser {
    NSURL *url = [self authorizeURL];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
        NSLog(@"%@ URL was opened : %d", url, success);
    }];
}

+ (NSURL *)authorizeURL {
    NSURL *baseURL = [NSURL URLWithString:AUTHAuthorizationHostname];
    NSURL *url = [NSURL URLWithString:AUTHAuthorizationEndpoint relativeToURL:baseURL];
    NSDictionary *parameters = @{
                                 @"response_type" : AUTHResponseType,
                                 @"client_id"     : AUTHApplicationId,
                                 @"device_id"     : [[NSUUID UUID] UUIDString],
                                 @"device_name"   : AUTHDeviceName
                                 };
    url = [[AUTHPathBuilder sharedBuilder] constructURL:url withParameters:parameters];
    
    return url;
}

@end
