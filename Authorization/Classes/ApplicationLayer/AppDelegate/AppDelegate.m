//
//  AppDelegate.m
//  Authorization
//
//  Created by Alexandr Orlov on 19.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AppDelegate.h"
#import "AUTHAuthorization.h"
#import "AUTHApplicationConfigurator.h"
#import "User+CoreDataClass.h"

static NSString * const kAuthorizationStoryboardName = @"Authorization";
static NSString * const kAlbumsStoryboardName        = @"Albums";

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIViewController *viewController = nil;
    [AUTHApplicationConfigurator setupCoreDataStack];
    viewController = [[UIStoryboard storyboardWithName:kAuthorizationStoryboardName bundle:nil] instantiateInitialViewController];
    self.window.rootViewController = viewController;
    if ([User currentUser]) {
        viewController = [[UIStoryboard storyboardWithName:kAlbumsStoryboardName bundle:nil] instantiateInitialViewController];
        self.window.rootViewController = viewController;
    }
    else {
        viewController = [[UIStoryboard storyboardWithName:kAuthorizationStoryboardName bundle:nil] instantiateInitialViewController];
        self.window.rootViewController = viewController;
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [[NSNotificationCenter defaultCenter] postNotificationName:AUTHDidBackFromAuthorizationNotificationName object:url];
    return YES;
}

@end
