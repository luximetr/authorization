//
//  AppDelegate.h
//  Authorization
//
//  Created by Alexandr Orlov on 19.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

