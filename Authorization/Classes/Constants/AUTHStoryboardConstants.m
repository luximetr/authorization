//
//  AUTHStoryboardConstants.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHStoryboardConstants.h"

#pragma mark - Segue Identifiers

NSString * const AUTHAlbumsViewControllerShowSegueIdentifier = @"albumsViewControllerShowSegueIdentifier";
NSString * const AUTHUnwindToAuthorizationViewControllerIdentifier = @"AUTHUnwindToAuthorizationViewControllerIdentifier";

#pragma mark - Cell Identifiers

NSString * const AUTHPhotoCollectionViewCellIdentifier       = @"AUTHPhotoCollectionViewCellIdentifier";
NSString * const AUTHAlbumCollectionViewCellIdentifier       = @"AUTHAlbumCollectionViewCellIdentifier";
