//
//  GeneralDefines.h
//  Authorization
//
//  Created by Alexandr Orlov on 24.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define SCREEN_HEIGHT_COEFFICIENT [[UIScreen mainScreen] bounds].size.height / 568.0
#define SCREEN_WIDTH_COEFFICIENT [[UIScreen mainScreen] bounds].size.width / 320.0
