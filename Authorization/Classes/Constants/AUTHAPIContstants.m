//
//  AUTHAPIContstants.m
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHAPIContstants.h"

NSString * const AUTHAuthorizationEndpoint           = @"/authorize";
NSString * const AUTHTokenReceiveEndpoint            = @"/token";
NSString * const AUTHServiceDocumentReceiveEndpoint  = @"api/me/";
