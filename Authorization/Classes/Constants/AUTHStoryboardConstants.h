//
//  AUTHStoryboardConstants.h
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Segue Identifiers

extern NSString * const AUTHAlbumsViewControllerShowSegueIdentifier;
extern NSString * const AUTHUnwindToAuthorizationViewControllerIdentifier;

#pragma mark - Cell Indentifier

extern NSString * const AUTHPhotoCollectionViewCellIdentifier;
extern NSString * const AUTHAlbumCollectionViewCellIdentifier;
