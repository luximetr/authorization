//
//  AUTHGeneralConstants.h
//  Authorization
//
//  Created by Alexandr Orlov on 24.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AUTHServerHostname;
extern NSString * const AUTHAuthorizationHostname;
extern NSString * const AUTHDidBackFromAuthorizationNotificationName;
extern NSString * const AUTHApplicationId;
extern NSString * const AUTHApplicationPassword;
extern NSString * const AUTHCallbackURLString;
extern NSString * const AUTHDeviceName;
