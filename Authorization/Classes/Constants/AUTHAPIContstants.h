//
//  AUTHAPIContstants.h
//  Authorization
//
//  Created by Alexandr Orlov on 25.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AUTHAuthorizationEndpoint;
extern NSString * const AUTHTokenReceiveEndpoint;
extern NSString * const AUTHServiceDocumentReceiveEndpoint;
