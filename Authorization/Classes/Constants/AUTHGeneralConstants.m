//
//  AUTHGeneralConstants.m
//  Authorization
//
//  Created by Alexandr Orlov on 24.10.16.
//  Copyright © 2016 Alexandr Orlov. All rights reserved.
//

#import "AUTHGeneralConstants.h"

NSString * const AUTHDidBackFromAuthorizationNotificationName = @"AUTHDidBackFromAuthorizationNotificationName";
NSString * const AUTHAuthorizationHostname = @"https://oauth.yandex.ru";
NSString * const AUTHServerHostname        = @"https://api-fotki.yandex.ru";
NSString * const AUTHApplicationId         = @"c247f9d1ed0e4412b9689f1c66afda33";
NSString * const AUTHApplicationPassword   = @"fb0af4874aa848fc8fa1533ebbeaceb0";
NSString * const AUTHCallbackURLString     = @"OrlovAuth://";
NSString * const AUTHDeviceName            = @"AuthDevice";
